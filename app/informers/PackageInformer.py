import logging
from package_providers.PackageProviderShell import PackageProviderShell
import config

logger = logging.getLogger("main")


class PackageInformer:
    def __init__(self, packages: dict):
        self._packages = packages
        logger.debug(packages)

    def get_all(self):
        """
        :return: {
                    <package name>: {
                        "systemd": {
                            <unit name>: {
                                "status": <status>,
                                "last_log"
                            }
                        }
                    }
                }
        """
        packages = {}
        if config.CONNECTOR == 'shell':
            provider = PackageProviderShell
        else:
            raise Exception('Connector ' + config.CONNECTOR + ' didn\'t match any known connector')

        for package_name, package_config in self._packages.items():
            packages[package_name] = {}
            logger.info(f'Looking for package: {package_name}')
            current_package_provider = provider(package_name, package_config)

            config_files_ok, config_files_list = current_package_provider.get_config()
            if config_files_ok:
                if len(config_files_list) > 0:
                    packages[package_name]['config'] = config_files_list
                logger.info('No config files found for: ' + package_name)
            else:
                logger.warning('Error occurred on search for config files: ' + ' '.join(config_files_list))

            systemd_unit_files_ok, systemd_unit_files_list = current_package_provider.get_systemd_unit_files()
            if systemd_unit_files_ok:
                if len(systemd_unit_files_list) > 0:
                    packages[package_name]['systemd_unit_files'] = systemd_unit_files_list
                logger.info('No systemd unit files found for: ' + package_name)
            else:
                logger.warning('Error occurred on search for systemd unit files: ' + ' '.join(systemd_unit_files_list))



            # packages[package_name]['systemd_status'] = current_package_provider.get_systemd_status()
            # packages[package_name]['crontabs'] = current_package_provider.get_crontabs()
        print(packages)
