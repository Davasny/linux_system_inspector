from connectors import shell
from package_providers.PackageProvider import PackageProvider


class PackageProviderShell(PackageProvider):
    def get_config(self):
        paths_list = self._package_config['config_paths'] + [self._default_config_path + '/' + self._package_name]
        paths_dict = {}

        for path in paths_list:
            path_exists = self._check_if_file_exists(path)
            if path_exists:
                result_ok, files_in_directory = self._get_files_from_directory(path)
                if result_ok:
                    for file_path in files_in_directory:
                        if self._file_is_allowed(file_path):
                            get_content, content = self._get_file_content(file_path)
                            if get_content:
                                paths_dict[file_path] = content
        return True, paths_dict

    def get_systemd_unit_files(self):
        result_ok, systemctl_output = shell.execute('systemctl list-unit-files --no-pager')

        if result_ok:
            systemd_unit_files = {}
            systemd_unit_names = [self._package_name] + self._package_config['systemd_unit_names']
            for line in systemctl_output:
                if any(name in line for name in systemd_unit_names):
                    unit_file = list(filter(None, line.split(' ')))
                    systemd_unit_files[unit_file[0]] = unit_file[1]
            return True, systemd_unit_files
        return False, systemctl_output

    def get_systemd_status(self):
        return NotImplemented

    def get_crontabs(self):
        return NotImplemented

    def _check_if_file_exists(self, path):
        result, content = shell.execute('stat ' + path)
        return result

    def _get_files_from_directory(self, path, recursive=True):
        """

        :param path:
        :param recursive:
        :return: absolute path to files
        """
        if recursive:
            result_ok, content = shell.execute('find ' + path)
        else:
            result_ok, content = shell.execute('find -maxdepth 1 ' + path)
        # todo: add output parser
        if not result_ok:
            return False, ' '.join(content)

        return True, content

    def _get_file_content(self, path):
        result, content = shell.execute('cat ' + path)
        return result, content
