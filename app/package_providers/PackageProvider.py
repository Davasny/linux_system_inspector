from lsi_setting import config
import re


class PackageProvider:
    def __init__(self, package_name: str, package_config={}):
        if 'config_paths' not in package_config:
            package_config['config_paths'] = []

        if 'systemd_unit_names' not in package_config:
            package_config['systemd_unit_names'] = []


        self._package_name = package_name
        self._package_config = package_config
        self._default_config_path = config['default_config_path']

    def get_config(self):
        """
        :return: {
                    <config path>: <content>,
                    <config path 2>: <content 2>,
                    <config path n>: <content n>
                 }
        """
        return NotImplemented

    def get_systemd_unit_files(self):
        """
        :return: {
                    <unit filename>: {
                        path: <path>,
                        content: <content>,
                    },
                    <unit filename 2>: {

                        path: <path 2>,
                        content: <content 2>,
                    }
                 }
        """
        return NotImplemented

    def get_systemd_status(self):
        """
        :return: {
                    <unit file>: <status>,
                    <unit file 2>: <status 2>,
                    <unit file n>: <status n>
                 }
        """
        return NotImplemented

    def get_crontabs(self):
        """
        :return: {
                <crontab path>: <content>,
                }
        """
        return NotImplemented

    def _file_is_allowed(self, file_path):
        # todo: use regex and add more files
        matched = re.findall(r'\.((conf(ig)?)|(cfg)|(json)|(ya?ml)|ini|xml)', file_path.split('/')[-1])
        if len(matched) > 0:
            return True
        return False
