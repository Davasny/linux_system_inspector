"""
Shell connector, executes command in current system as current user and return list of lines from command output
"""
import logging
from subprocess import Popen, PIPE

logger = logging.getLogger('main')

def execute(command):
    """

    :param command: str, command to be executed in system
    :return: list, output lines from command
    """
    logger.debug('Trying command: ' + command)
    try:
        process = Popen(command.split(' '), stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()

        if process.returncode != 0:
            message = stderr.decode('utf-8').splitlines()
            logger.debug('Command "' + command + '" failed (code '+ str(process.returncode) + '): "' + ' '.join(message) + '"')
            return False, message

        return True, stdout.decode('utf-8').splitlines()
    except OSError:
        message = 'Command ' + command + ' not found'
        logger.warning(message)
        return False, message
    except FileNotFoundError:
        # mainly for windows exceptions
        message = 'Command ' + command + ' not found'
        logger.warning(message)
        return False, message
