import yaml
from os import path, getcwd

CONFIG_FILENAME = 'lsi_setting.yml'

if path.isfile(CONFIG_FILENAME):
    with open(CONFIG_FILENAME, 'r', encoding='utf8') as file:
        config = yaml.safe_load(file.read())
else:
    raise Exception(f'File config not found: {getcwd()}/{CONFIG_FILENAME}')
