import logging
import config


def set_logger(name):
    level = config.LOG_LEVEL

    log_level = logging.INFO
    if level == "DEBUG":
        log_level = logging.DEBUG
    elif level == "WARNING":
        log_level = logging.WARNING
    elif level == "ERROR":
        log_level = logging.ERROR
    elif level == "CRITICAL":
        log_level = logging.CRITICAL

    logger = logging.getLogger(name)
    logger.setLevel(log_level)

    # setup steam handler
    sh = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(module)s - %(message)s')
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    return logger
