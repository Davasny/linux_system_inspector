from lsi_setting import config
import log
from informers.PackageInformer import PackageInformer

logger = log.set_logger("main")

if __name__ == '__main__':
    logger.debug(config)
    packages = PackageInformer(config['packages'])

    result = {
        'packages': packages.get_all()
    }
